# Mobile Application Development - Spring 2023 - Monday Section

## Lecture # 01
OOP Concepts, Android Framework, Different App Development Frameworks, Android Studio Setup & Configuration

## Lecture # 02	IDE Features, Hello World App, Android App File Structure, Gradle Build Tool System	

## Lecture # 03	Android App Components, Multiscreen App & Navigation, Data Passing Strategies	

## Lecture # 04	Responsive Apps, Layouts, Widgets, Multi-mode Apps	

## Lecture # 05	Culture Specific Apps, Color Palette, Themes, Localisation	

## Lecture # 06	Data Visualisation (Recycler View), Persistent Storage (Shared Preferences)		

## Lecture # 07	Android Room Persistence Storage (Local Apps)	

## Lecture # 08	Midterm Exam

## Lecture # 09	Server driven Apps Part - 1 (Apache + PHP + MySQL + Android App)	

## Lecture # 10	Server driven Apps Part - 2 (Node/Express + MongoDB/Postgres + Android App)		

## Lecture # 11	App Distribution Strategies (Play Store, Huawei App Gallery), APK vs AAB, Split APK, Dynamic Delivery	

## Lecture # 12	Building Mobile App using React Native Framework, Framework Setup & Configuration, Hello world App	

## Lecture # 13	Use case Implementation Part 1

## Lecture # 14	Use case Implementation Part 2	

## Lecture # 15	Final Exam